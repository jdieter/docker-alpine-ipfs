FROM ipfs/go-ipfs as ipfs

FROM python:3.6-alpine

COPY --from=ipfs /usr/local/bin/ipfs /usr/local/bin
RUN mkdir /lib64 && ln -s /lib/libc.musl-x86_64.so.1 /lib64/ld-linux-x86-64.so.2
