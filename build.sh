#!/bin/sh

if [[ ! -d '.git' ]];then
	cd ..
fi

git pull
if [[ "$?" != "0" ]];then
	echo ARGH!
	exit 1
fi

# We currently use buildah to build the docker images because the version of
# docker in Fedora isn't able to build multi-stage Dockerfiles.  If you have
# trouble with the buildah push command, update to buildah-1.4-3 (or possibly
# buildah-1.4-2) in the Fedora updates-testing repositories

buildah bud -f Dockerfile --format=docker -t iwe-alpine-ipfs .
#buildah push alpine-ipfs
